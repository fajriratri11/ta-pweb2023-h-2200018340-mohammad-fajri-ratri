    const form = document.querySelector('form');
    const nama = document.querySelector('input[name="nama"]');
    const nim = document.querySelector('input[name="NIM"]');
    const kelas = document.querySelector('select');
    const nohp = document.querySelector('input[name="No.Hp"]');
    const submit = document.querySelector('input[type="submit"]');

   
    form.addEventListener('submit', (event) => {
        event.preventDefault(); 
        const errors = [];
        if (nama.value === '') {
            errors.push('Nama harus diisi');
        }
        if (nim.value === '') {
            errors.push('NIM harus diisi');
        }
        if (kelas.value === 'Pilih Kelas') {
            errors.push('Kelas harus dipilih');
        }
        if (nohp.value === '') {
            errors.push('No. HP harus diisi');
        } else if (isNaN(nohp.value)) {
            errors.push('No. HP harus berupa angka');
        }
        if (errors.length > 0) {
            alert(errors.join('\n')); 
        } else {
            alert('Data berhasil dikirim'); 
            form.reset(); 
        }
    });

    
    form.addEventListener('input', () => {
        submit.disabled = (nama.value === '' || nim.value === '' || kelas.value === 'Pilih Kelas' || nohp.value === '');
    });


